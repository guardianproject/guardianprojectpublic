#!/bin/bash

set -e

if ! command -v inkscape &> /dev/null; then
  echo "inkscape not installed. aborting"
  exit 1
fi

widths=(64 128 256 512)
mkdir -p pngs
for svg in *.svg; do
  for width in "${widths[@]}"; do
    filename=$(basename -- "$svg")
    filename="${filename%.*}"
    inkscape "$svg" --export-type=png --export-filename="pngs/$filename-w$width.png" --export-width="$width"
  done
done
